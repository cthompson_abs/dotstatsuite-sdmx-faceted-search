import 'core-js/stable'
import 'regenerator-runtime/runtime'
import { initResources } from 'jeto'
import debug from './debug'
import initSolr from './solr'
import initConfig from './init/config'

const resources = [
  initConfig,
  initSolr,
]

initResources(resources)
  .then(ctx => {
    ctx().solr.manageSchema().then(() =>  process.exit())
  })
  .catch(err => {
    debug.error(err)
    process.exit()
  })

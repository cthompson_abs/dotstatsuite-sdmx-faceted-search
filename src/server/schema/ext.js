import { SFS_TEXT_EXT, SFS_TEXT_LIST_EXT } from '../sdmx/utils'

export const TEXTFIELD_EXT = 't'
export const STRING_EXT = 's'
export const STRING_LIST_EXT = 'ss'
export const TEXT_LIST_EXT = SFS_TEXT_LIST_EXT
export const TEXT_EXT = SFS_TEXT_EXT
export const INT_EXT = 'i'
export const DOUBLE_EXT = 'd'

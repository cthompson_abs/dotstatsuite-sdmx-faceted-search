import R from 'ramda';
import initSolr from '..';
import { manageSchema, tearContext, initConfig } from '../../tests/utils';
import data from './fixtures/data.json';

let CTX;
const tc = () => CTX
let TENANT

describe('Solr Data', function() {
  beforeAll(() =>
    initConfig()
    .then(initSolr)
    .then(async ctx => {
      await manageSchema(ctx().TENANT, ctx().solr)
      return ctx
    })
    .then(ctx => {
      CTX = ctx
      TENANT = CTX().TENANT
    }),
  );
  afterAll(tearContext(tc));

  it('should delete and load all data', function(done) {
    const { solr } = CTX();
    const solrClient = solr.getClient(TENANT);
    solrClient
      .deleteAll()
      .then(() => solrClient.add(data))
      .then(() => solrClient.select({ query: '*:*' }))
      .then(({ response: { docs } }) => {
        expect(data).toEqual(R.map(R.omit(['_version_']), docs));
        done();
      });
  });

  it('should filter data by id', function(done) {
    const { solr } = CTX();
    const id = data[0].id;
    const solrClient = solr.getClient(TENANT);
    solrClient.select({ query: `id:${id}` }).then(({ response: { docs } }) => {
      expect(docs[0].id).toEqual(id);
      done();
    });
  });
});

import { omit } from 'ramda'
import debug from '../../debug'
import { checkTenant, getSolrConfig } from '../utils'

const NAME = 'config'

export const service = {
  async delete() {
    const { tenant, solrConfig } = this.locals
    const { configManager } = this.globals()
    const {
      dataflows: { locales },
    } = solrConfig
    debug.error(`deleting all stored config (mongo)`)
    await configManager.reset(tenant, locales)
    return {}
  },

  get() {
    const { solrConfig } = this.locals
    return Promise.resolve(omit(['apiKey'], solrConfig))
  },
}

const init = evtx => {
  evtx.use(NAME, service)
  evtx.service(NAME).before({
    delete: [checkTenant, getSolrConfig],
    get: [checkTenant, getSolrConfig],
  })
}

export default init

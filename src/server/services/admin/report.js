const NAME = 'report';
export const service = {
  get() {
    const { report } = this.globals();
    return Promise.resolve(report());
  },
};

const init = evtx => {
  evtx.use(NAME, service).service(service.name);
};

export default init;

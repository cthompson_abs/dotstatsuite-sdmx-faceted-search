import { encode, decode } from '..';

describe('server | utils', () => {
  it('should encode', () => {
    const name = 'NAME';
    expect(decode(encode(name))).toEqual(name);
  });
});

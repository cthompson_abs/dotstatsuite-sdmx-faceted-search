import 'core-js/stable'
import 'regenerator-runtime/runtime'
import { initResources } from 'jeto'
import debug from './debug'
import initHttp from './init/http'
import initServices from './services'
import initSolr from './solr'
import initConfig from './init/config'
import initConfigManager from './init/configManager'
import initCachedfacets from './init/cachedFacets'
import initReporter from './init/reporter'
import initIndexer from './init/indexer'
import initParams from './init/loadParams'
import initMongo from './init/mongo'
import initModels from './init/models'
import initLogger from './init/logger'

const resources = [
  initConfig,
  ctx => {
    debug.info(ctx().config, 'running config')
    return ctx
  },
  initMongo,
  initModels,
  initReporter,
  initLogger,
  initConfigManager,
  initSolr,
  initParams,
  initCachedfacets,
  initIndexer,
  initServices,
  initHttp,
  async ctx => {
    const { configProvider } = ctx()
    const members = await configProvider.getTenants()
    debug.info(`Available members: [${Object.keys(members).join(', ')}]`)
    return ctx
  },
  ctx => {
    debug.info(ctx().config, 'running config')
    return ctx
  },
]

initResources(resources)
  .then(() => debug.info('🚀 server started'))
  .catch(err => {
    debug.error(err)
    process.exit()
  })

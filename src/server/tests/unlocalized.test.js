import axios from 'axios'
import nock from 'nock'
import urljoin from 'url-join'
import { createContext } from 'jeto'
import initHttp from '../init/http'
import initServices from '../services'
import initSolr from '../solr'
import initIndexer from '../init/indexer'
import initReporter from '../init/reporter'
import initConfigManager from '../init/configManager'
import initParams from '../init/loadParams'
import { waitFor, manageSchema, tearContext, initConfig } from './utils'
import initMongo from '../init/mongo'
import initModels from '../init/models'
import initLogger from '../init/logger'

const df1 = require('./mocks/sdmx/df1')
const df2 = require('./mocks/sdmx/df2')

let TENANT
let CTX
const tc = () => CTX

const makeRequestFactory = (http, { apiKey }) => ({ method = 'post', url, data }) =>
  axios({ method, url: urljoin(http.url, url), data, headers: { 'x-tenant': TENANT.id, 'x-api-key': apiKey } })
    .then(res => [res.status, res.data])
    .catch(res => [res.response.status, res.response.data])

let makeRequest

const params = () => ({
  defaultLocale: 'en',
})

const initContext = async () => {
  try {
    CTX = await initConfig(createContext, params())
      .then(ctx => {
        TENANT = ctx().TENANT
        return ctx
      })
      .then(initMongo)
      .then(initModels)
      .then(initReporter)
      .then(initLogger)
      .then(initSolr)
      .then(initConfigManager)
      .then(initParams)
      .then(initIndexer)
      .then(initServices)
      .then(initHttp)
    await manageSchema(TENANT, CTX().solr)
    const solrClient = CTX().solr.getClient(TENANT)
    await solrClient.deleteAll()
    await CTX().configManager.updateSchema(TENANT, CTX().config.fields)
    makeRequest = makeRequestFactory(CTX().http, CTX().config)
  } catch (e) {
    throw e
  }
}

beforeAll(initContext)
afterAll(tearContext(tc))

describe('Server | services | admin', () => {
  const email = 'EMAIL'

  it('should not index dataflow', async () => {
    const { models } = CTX()
    const dataSpaceId = 'demo-stable'
    const space = TENANT.spaces[dataSpaceId]

    await models.jobs.deleteAll()

    const dataflow = { agencyID: 'TEST', id: 'DF1', version: '1.0' }
    const dataflows = [dataflow]

    nock(space.url)
      .get('/categoryscheme/OECD/OECDCS1/1.0?references=dataflow&detail=full')
      .reply(200, { data: { dataflows } })

    nock(space.url)
      .get('/categoryscheme/OECD/TEST/1.0?references=dataflow&detail=full')
      .reply(200, { data: {} })

    nock(space.url)
      .get('/dataflow/TEST/DF1/1.0/?references=all&detail=referencepartial')
      .reply(200, df1)

    const [status, res] = await makeRequest({
      method: 'POST',
      url: `/admin/dataflow?userEmail=${email}&mode=sync`,
      data: { dataSpaceId, ...dataflow },
    })

    expect(status).toEqual(409)
    expect(res[0]['demo-stable'].dataflows.rejected).toEqual(1)

    let logs
    await waitFor(async () => {
      logs = await models.jobs.loadAll()
      expect(logs[0].outcome).toEqual('warning')
    })
    expect(logs[0].logs[0].status).toEqual('warning')
    // console.log(JSON.stringify(logs, null, 4))
  })

  it('should index partially', async () => {
    const { models } = CTX()
    const dataSpaceId = 'demo-stable'
    const space = TENANT.spaces[dataSpaceId]

    await models.jobs.deleteAll()

    const dataflow = { agencyID: 'TEST', id: 'DF2', version: '1.0' }
    const dataflows = [dataflow]

    nock(space.url)
      .get('/categoryscheme/OECD/OECDCS1/1.0?references=dataflow&detail=full')
      .reply(200, { data: { dataflows } })

    nock(space.url)
      .get('/categoryscheme/OECD/TEST/1.0?references=dataflow&detail=full')
      .reply(200, { data: {} })

    nock(space.url)
      .get('/dataflow/TEST/DF2/1.0/?references=all&detail=referencepartial')
      .reply(200, df2)

    const [status] = await makeRequest({
      method: 'POST',
      url: `/admin/dataflow?userEmail=${email}&mode=sync`,
      data: { dataSpaceId, ...dataflow },
    })

    expect(status).toEqual(207)
  })

  it('should get french fields', async () => {
    const search = ' dataflowId:"DF2"'
    const lang = 'en'
    const [, res] = await makeRequest({ method: 'POST', url: '/api/search', data: { lang, search } })
    // console.log(JSON.stringify(res, null, 4))
    expect(res.numFound).toEqual(0)
  })
})

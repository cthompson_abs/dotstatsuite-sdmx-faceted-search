import axios from 'axios'
import nock from 'nock'
import { prop } from 'ramda'
import urljoin from 'url-join'
import { createContext } from 'jeto'
import initHttp from '../init/http'
import initServices from '../services'
import initSolr from '../solr'
import initIndexer from '../init/indexer'
import initReporter from '../init/reporter'
import initConfigManager from '../init/configManager'
import initParams from '../init/loadParams'
import initMongo from '../init/mongo'
import { tearContext, initConfig } from './utils'

const DF_GENDER = require('./mocks/sdmx/uncoded.json')

const makeRequestFactory = (http, { apiKey }) => ({ method = 'post', url, data }) =>
  axios({ method, url: urljoin(http.url, url), data, headers: { 'x-tenant': TENANT.id, 'x-api-key': apiKey } }).then(
    prop('data'),
  )
let makeRequest
let CTX
let TENANT
const tc = () => CTX

const initContext = async () => {
  try {
    CTX = await initConfig(createContext)
      .then(ctx => {
        TENANT = ctx().TENANT
        return ctx
      })
      .then(initMongo)
      .then(initReporter)
      .then(initSolr)
      .then(async ctx => {
        await ctx().solr.manageSchema()
        const solrClient = ctx().solr.getClient(TENANT)
        await solrClient.deleteAll()
        return ctx
      })
      .then(initConfigManager)
      .then(initParams)
      .then(initIndexer)
      .then(initServices)
      .then(initHttp)
    makeRequest = makeRequestFactory(CTX().http, CTX().config)
  } catch (e) {
    console.error(e) // eslint-disable-line
    throw e
  }
}

beforeAll(initContext)
afterAll(tearContext(tc))

describe('Server | services | admin', () => {
  it('should get config', async () => {
    const res = await makeRequest({ method: 'GET', url: '/admin/config' })
    expect(res.fields['lorder'].type).toEqual('ATTR')
    expect(res.dataflows.schema.find(x => x.name === 'lorder')).toBeDefined()
  })

  it('should create a dataflow', async () => {
    const spaceId = 'demo-stable'
    const space = TENANT.spaces[spaceId]

    const { id, version, agencyID: agencyId } = DF_GENDER.data.dataflows[0]

    const dataflows = [
      {
        id,
        agencyId,
        version,
      },
    ]

    nock(space.url)
      .get('/categoryscheme/OECD/OECDCS1/1.0?references=dataflow&detail=full')
      .reply(200, { data: { dataflows } })

    nock(space.url)
      .get('/categoryscheme/OECD/TEST/1.0?references=dataflow&detail=full')
      .reply(200, { data: {} })

    nock(space.url)
      .get('/dataflow/OECD.DCD.FSD/DSD_GNDR@DF_GENDER/1.0/?references=all&detail=referencepartial')
      .reply(200, DF_GENDER)

    await makeRequest({
      method: 'POST',
      url: '/admin/dataflow',
      data: { spaceId, id, version, agencyId },
    })

    const search = ` dataflowId:"${id}" version:"${version}" agencyId:"${agencyId}"`
    const res = await makeRequest({ method: 'POST', url: '/api/search', data: { search } })
    expect(res.numFound).toEqual(1)
  })
})

import axios from 'axios'
import nock from 'nock'
import { prop } from 'ramda'
import urljoin from 'url-join'
import { createContext } from 'jeto'
import initHttp from '../init/http'
import initServices from '../services'
import initSolr from '../solr'
import initIndexer from '../init/indexer'
import initReporter from '../init/reporter'
import initConfigManager from '../init/configManager'
import initParams from '../init/loadParams'
import initMongo from '../init/mongo'
import { manageSchema, tearContext, initConfig } from './utils'

const orderDataflowFunc = require('./mocks/sdmx/order')
const orderOfYGE25 = 10420

const makeRequestFactory = (http, { apiKey }) => ({ method = 'post', url, data }) =>
  axios({ method, url: urljoin(http.url, url), data, headers: { 'x-tenant': TENANT.id, 'x-api-key': apiKey } }).then(
    prop('data'),
  )
let makeRequest
let CTX
let TENANT
const tc = () => CTX

const initContext = async () => {
  try {
    CTX = await initConfig(createContext)
      .then(ctx => {
        TENANT = ctx().TENANT
        return ctx
      })
      .then(initMongo)
      .then(initReporter)
      .then(initSolr)
      .then(async ctx => {
        await manageSchema(TENANT, ctx().solr)
        const solrClient = ctx().solr.getClient(TENANT)
        await solrClient.deleteAll()
        return ctx
      })
      .then(initConfigManager)
      .then(initParams)
      .then(initIndexer)
      .then(initServices)
      .then(initHttp)
    makeRequest = makeRequestFactory(CTX().http, CTX().config)
  } catch (e) {
    console.error(e) // eslint-disable-line
    throw e
  }
}

beforeAll(initContext)
afterAll(tearContext(tc))

const indexDataflow = async customOrder => {
  const dataSpaceId = 'demo-stable'
  const space = TENANT.spaces[dataSpaceId]
  const dataflow = orderDataflowFunc(customOrder)

  const { id, version, agencyID: agencyId } = dataflow.data.dataflows[0]
  const dataflows = [{ id, agencyId, version }]

  nock(space.url)
    .get('/categoryscheme/OECD/OECDCS1/1.0?references=dataflow&detail=full')
    .reply(200, { data: { dataflows } })

  nock(space.url)
    .get('/categoryscheme/OECD/TEST/1.0?references=dataflow&detail=full')
    .reply(200, { data: {} })

  nock(space.url)
    .get('/dataflow/ILO/DF_DATABASE_530/1.0/?references=all&detail=referencepartial')
    .reply(200, dataflow)

  await makeRequest({
    method: 'POST',
    url: '/admin/dataflow',
    data: { dataSpaceId, id, version, agencyId },
  })

  const search = `dataflowId:"${id}" version:"${version}" agencyId:"${agencyId}"`
  const res = await makeRequest({ method: 'POST', url: '/api/search', data: { search } })

  return res
}

describe('Server | services | admin', () => {
  it('should get config', async () => {
    const res = await makeRequest({ method: 'GET', url: '/admin/config' })
    expect(res.fields['lorder'].type).toEqual('ATTR')
    expect(res.dataflows.schema.find(x => x.name === 'lorder')).toBeDefined()
  })

  it(`should index a dataflow with annotation ORDER AGE_YTHADULT_YGE25 set to ${orderOfYGE25}`, async () => {
    const res = await indexDataflow(orderOfYGE25)
    expect(res.numFound).toEqual(1)
  })

  it('should filter on 1 facet', async () => {
    const search = ``
    const facets = {
      datasourceId: ['demo-stable'],
      Sex: ['0|Female#SEX_F#'],
    }
    const res = await makeRequest({ method: 'POST', url: '/api/search', data: { search, facets } })
    expect(res.numFound).toEqual(1)
  }, 500)

  it('should match orders on facets/values', async () => {
    const allValues = new Set([
      { val: 'demo-stable', order: undefined },
      { val: '0|SDG labour market indicators#SDG#', order: 110 },
      {
        val: '1|SDG labour market indicators#SDG#|Annual indicators#YI#',
        order: 120,
      },
      { val: '0|Afghanistan#AFG#', order: 10000 },
      {
        val: '0|[1.1.1] Proportion of population below the international poverty line#SDG_0111_RT#',
        order: 111,
      },
      { val: '0|Female#SEX_F#', order: 10030 },
      { val: '0|25+#AGE_YTHADULT_YGE25#', order: orderOfYGE25 },
      { val: '0|cat1-en#CAT1#', order: 650 },
      { val: '1|cat1-en#CAT1#|cat11-en#CAT11#', order: 750 },
    ])

    const search = ``
    const facets = {
      datasourceId: ['demo-stable'],
    }
    const res = await makeRequest({ method: 'POST', url: '/api/search', data: { search, facets } })
    expect(res.numFound).toEqual(1)
    const orders = []
    for (const facetId in res.facets) {
      for (const x of res.facets[facetId].buckets) {
        orders.push({ val: x.val, order: x.order })
      }
    }
    expect(new Set(orders)).toEqual(allValues)
  }, 500)

  it('should reset config', async () => {
    await makeRequest({ method: 'DELETE', url: '/admin/config' })
    const res = await makeRequest({ method: 'GET', url: '/admin/config' })
    expect(res.dataflows.locales).toBeDefined()
  })

  it('should index a dataflow with annotation ORDER AGE_YTHADULT_YGE25 set to undefined', async () => {
    const res = await indexDataflow()
    expect(res.numFound).toEqual(1)
  })

  it('should match no order on annotation ORDER AGE_YTHADULT_YGE25', async () => {
    const search = ``
    const facets = {
      datasourceId: ['demo-stable'],
    }
    const res = await makeRequest({ method: 'POST', url: '/api/search', data: { search, facets } })
    expect(res.numFound).toEqual(1)
    let order
    for (const facetId in res.facets) {
      for (const x of res.facets[facetId].buckets) {
        if (x.val === '0|25+#AGE_YTHADULT_YGE25#') order = x.order
      }
    }
    expect(order).toEqual(undefined)
  }, 500)

  it('should re-index the dataflow with annotation ORDER AGE_YTHADULT_YGE25 reset to original value', async () => {
    const res = await indexDataflow(orderOfYGE25)
    expect(res.numFound).toEqual(1)
  })

  it('should match orignal order on annotation ORDER AGE_YTHADULT_YGE25', async () => {
    const search = ``
    const facets = {
      datasourceId: ['demo-stable'],
    }
    const res = await makeRequest({ method: 'POST', url: '/api/search', data: { search, facets } })
    expect(res.numFound).toEqual(1)
    let order
    for (const facetId in res.facets) {
      for (const x of res.facets[facetId].buckets) {
        if (x.val === '0|25+#AGE_YTHADULT_YGE25#') order = x.order
      }
    }
    expect(order).toEqual(orderOfYGE25)
  }, 500)
})

import axios from 'axios'
import nock from 'nock'
import { prop } from 'ramda'
import urljoin from 'url-join'
import { createContext } from 'jeto'
import initHttp from '../init/http'
import initServices from '../services'
import initSolr from '../solr'
import initIndexer from '../init/indexer'
import initReporter from '../init/reporter'
import initConfigManager from '../init/configManager'
import initParams from '../init/loadParams'
import fixture from './fixtures/data1'
import { manageSchema, tearContext, initConfig, waitFor } from './utils'
import initMongo from '../init/mongo'
import initModels from '../init/models'
import initLogger from '../init/logger'

const IRS = require('./mocks/sdmx/IRS')

let TENANT
let CTX
const tc = () => CTX

const makeRequestFactory = (http, { apiKey }) => ({ method = 'post', url, data }) =>
  axios({ method, url: urljoin(http.url, url), data, headers: { 'x-tenant': TENANT.id, 'x-api-key': apiKey } }).then(
    prop('data'),
  )
let makeRequest

const params = () => ({
  defaultLocale: 'en',
})

const initContext = async () => {
  try {
    CTX = await initConfig(createContext, params())
      .then(ctx => {
        TENANT = ctx().TENANT
        return ctx
      })
      .then(initMongo)
      .then(initModels)
      .then(initReporter)
      .then(initLogger)
      .then(initSolr)
      .then(initConfigManager)
      .then(initParams)
      .then(initIndexer)
      .then(initServices)
      .then(initHttp)
    await manageSchema(TENANT, CTX().solr)
    const solrClient = CTX().solr.getClient(TENANT)
    await solrClient.deleteAll()
    await CTX().configManager.updateSchema(TENANT, CTX().config.fields)
    makeRequest = makeRequestFactory(CTX().http, CTX().config)
  } catch (e) {
    throw e
  }
}

beforeAll(initContext)
afterAll(tearContext(tc))

describe('Server | services | admin', () => {
  const email = 'EMAIL'
  let requestId

  it('should not index catategoryscheme', async () => {
    const { models } = CTX()
    try {
      await makeRequest({ method: 'POST', url: `/admin/dataflows?mode=sync&userEmail=${email}` })
    } catch (error) {
      let logs
      await waitFor(async () => {
        logs = await models.jobs.loadAll(TENANT)
        expect(logs[0].executionStatus).toEqual('completed')
      })

      const data = logs[0]
      expect(data.userEmail).toEqual(email)
      expect(data.tenant).toEqual('test')
      expect(data.action).toEqual('indexAll')
      expect(data.dataSourceIds).toEqual(['qa-stable', 'demo-stable'])
      expect(data.logs.length).toEqual(3)
    }
  })

  it('should get logs with errors', async () => {
    const params = {
      userEmail: email,
      dataSpaceId: 'demo-stable',
      submissionStart: new Date([2017]),
      submissionEnd: new Date([new Date().getUTCFullYear() + 1]),
      executionStatus: 'completed',
      executionOutcome: 'error',
      artefact: {
        resourceType: 'categoryscheme',
        agencyID: 'OECD',
        id: 'TEST',
        version: '1.0',
      },
    }
    let logs = await makeRequest({ method: 'POST', data: params, url: `/admin/logs` })
    const data = logs[0]
    expect(data.logs.length).toEqual(3)
  })

  it('should get catategoryscheme but not index dataflow', async () => {
    const { dataflowId_s: id, version_s: version, agencyId_s: agencyID } = fixture.data[0]
    const dataflows = [{ id, agencyID, version }]

    nock('http://qa-stable')
      .persist()
      .get('/categoryscheme/OECD/OECDCS1/1.0/?references=dataflow')
      .reply(200, { data: {} })

    nock('http://demo-stable')
      .persist()
      .get('/categoryscheme/OECD/OECDCS1/1.0/?references=dataflow')
      .reply(200, { data: { dataflows } })

    nock('http://demo-stable')
      .persist()
      .get('/categoryscheme/OECD/TEST/1.0/?references=dataflow')
      .reply(200, { data: {} })

    const { models } = CTX()
    await models.jobs.deleteAll()
    try {
      await makeRequest({ method: 'POST', url: `/admin/dataflows?mode=sync&userEmail=${email}` })
    } catch (error) {
      let logs
      await waitFor(async () => {
        logs = await models.jobs.loadAll(TENANT)
        expect(logs[0].executionStatus).toEqual('completed')
      })
      // console.log(JSON.stringify(logs, null, 4))
      expect(logs[0].logs[0].artefact).toEqual({ resourceType: 'dataflow', agencyID: 'ECB', id: 'IRS', version: '1.0' })
    }
  })

  it('should index many dataflows', async () => {
    nock('http://demo-stable')
      .persist()
      .get('/dataflow/ECB/IRS/1.0/?references=all&detail=referencepartial')
      .reply(200, IRS)

    const { models } = CTX()
    await models.jobs.deleteAll()
    await makeRequest({ method: 'POST', url: `/admin/dataflows?mode=sync&userEmail=${email}` })
    let logs
    await waitFor(async () => {
      logs = await models.jobs.loadAll(TENANT)
      expect(logs[0].executionStatus).toEqual('completed')
    })
    const res = await makeRequest({ method: 'GET', url: `/admin/logs` })
    expect(logs[0].logs[0].artefact).toEqual({ resourceType: 'dataflow', agencyID: 'ECB', id: 'IRS', version: '1.0' })
    expect(res[0].outcome).toEqual('success')
  })

  it('should get logs in success', async () => {
    const params = {
      userEmail: email,
      dataSpaceId: 'demo-stable',
      submissionStart: new Date([2017]),
      submissionEnd: new Date([new Date().getUTCFullYear() + 1]),
      executionStatus: 'completed',
      executionOutcome: 'success',
    }
    let logs = await makeRequest({ method: 'POST', data: params, url: `/admin/logs` })
    requestId = logs[0].id
    expect(logs.length).toEqual(1)
  })

  it('should load one log', async () => {
    const log = await makeRequest({ method: 'GET', url: `/admin/logs?id=${requestId}` })
    expect(log.id).toEqual(requestId)
  })

  it('should index one dataflow', async () => {
    const { models } = CTX()
    const dataSpaceId = 'demo-stable'
    const space = TENANT.spaces[dataSpaceId]

    await models.jobs.deleteAll()

    const { dataflowId_s: id, agencyId_s: agencyId } = fixture.data[0]

    const version = '1.4'
    const dataflows = [
      {
        id,
        agencyId,
        version,
      },
    ]

    nock(space.url)
      .get('/categoryscheme/OECD/OECDCS1/1.0?references=dataflow&detail=full')
      .reply(200, { data: { dataflows } })

    nock(space.url)
      .get('/categoryscheme/OECD/TEST/1.0?references=dataflow&detail=full')
      .reply(200, { data: {} })

    IRS.data.dataflows[0].version = version
    nock(space.url)
      .get('/dataflow/ECB/IRS/1.4/?references=all&detail=referencepartial')
      .reply(200, IRS)

    await makeRequest({
      method: 'POST',
      url: `/admin/dataflow?userEmail=${email}`,
      data: { dataSpaceId, id, version, agencyId },
    })

    let logs
    await waitFor(async () => {
      logs = await models.jobs.loadAll(TENANT)
      expect(logs[0].executionStatus).toEqual('completed')
    })
    const res = await makeRequest({ method: 'GET', url: `/admin/logs` })
    expect(res[0].logs[0].artefact).toEqual({ resourceType: 'dataflow', agencyID: 'ECB', id: 'IRS', version: '1.4' })
    expect(res[0].action).toEqual('indexOne')
    expect(res[0].outcome).toEqual('success')
  })

  it('should delete a dataflow', async () => {
    const { models } = CTX()
    const dataSpaceId = 'demo-stable'
    const { dataflowId_s: id, agencyId_s: agencyId } = fixture.data[0]
    const version = '1.4'

    await models.jobs.deleteAll()

    await makeRequest({
      method: 'DELETE',
      url: `/admin/dataflow?userEmail=${email}`,
      data: { dataSpaceId, id, version, agencyId },
    })
    let logs
    await waitFor(async () => {
      logs = await models.jobs.loadAll(TENANT)
      expect(logs[0].executionStatus).toEqual('completed')
    })

    const res = await makeRequest({ method: 'GET', url: `/admin/logs` })
    expect(res[0].action).toEqual('deleteOne')
    expect(res[0].outcome).toEqual('success')
  })

  it('should not delete a dataflow', async () => {
    const { models } = CTX()
    const dataSpaceId = 'demo-stable'
    const { dataflowId_s: id, agencyId_s: agencyId } = fixture.data[0]
    const version = '1.4'

    await models.jobs.deleteAll()

    await makeRequest({
      method: 'DELETE',
      url: `/admin/dataflow?userEmail=${email}`,
      data: { dataSpaceId, id, version, agencyId },
    }).catch(() => null)

    let logs
    await waitFor(async () => {
      logs = await models.jobs.loadAll(TENANT)
      expect(logs[0].executionStatus).toEqual('completed')
    })

    const res = await makeRequest({ method: 'GET', url: `/admin/logs` })
    expect(res[0].action).toEqual('deleteOne')
    expect(res[0].outcome).toEqual('warning')
  })

  it('should delete all dataflows', async () => {
    const { models } = CTX()
    const dataSpaceId = 'demo-stable'
    const { dataflowId_s: id, agencyId_s: agencyId } = fixture.data[0]
    const version = '1.4'

    await models.jobs.deleteAll()

    await makeRequest({
      method: 'DELETE',
      url: `/admin/dataflows`,
      data: { dataSpaceId, id, version, agencyId, userEmail: email },
    })

    let logs
    await waitFor(async () => {
      logs = await models.jobs.loadAll(TENANT)
      expect(logs[0].executionStatus).toEqual('completed')
    })

    const res = await makeRequest({ method: 'GET', url: `/admin/logs` })
    expect(res[0].action).toEqual('deleteAll')
    expect(res[0].tenant).toEqual(TENANT.id)
    expect(res[0].outcome).toEqual('success')
    expect(res[0].logs.length).toEqual(1)
  })

  it('should get deleteAll logs', async () => {
    const res = await makeRequest({ method: 'GET', url: `/admin/logs?action=deleteAll&userEmail=EMAIL` })
    expect(res.length).toEqual(1)
  })

  it('should manage outcome', async () => {
    const { models } = CTX()
    await models.jobs.startIndexing({ loadingId: 1, userEmail: 'me', tenant: TENANT, action: 'indexAll' })
    let logs = await models.jobs.loadAll(TENANT, { id: 1, executionStatus: 'queued' })
    expect(logs[0].outcome).toEqual('none')

    await models.jobs.startLoadingDatasource({ loadingId: 1, datasourceId: 1, dataSpaceId: 1 })
    logs = await models.jobs.loadAll(TENANT, { id: 1, executionStatus: 'inProgress' })
    expect(logs[0].outcome).toEqual('none')

    await models.jobs.manageError(new Error('ERROR'), {
      loadingId: 1,
      dataSpaceId: 1,
      message: 'message',
      logger: 'logger',
    })
    logs = await models.jobs.loadAll(TENANT, { id: 1, executionStatus: 'inProgress' })
    expect(logs[0].outcome).toEqual('error')

    await models.jobs.manageWarning({ loadingId: 1, dataSpaceId: 1, message: 'message', logger: 'logger' })
    logs = await models.jobs.loadAll(TENANT, { id: 1, executionStatus: 'inProgress' })
    expect(logs[0].outcome).toEqual('multiStatus')
  })

  it('should manage dataSpaceId', async () => {
    const { models } = CTX()
    await models.jobs.loadAll(TENANT)
    // console.log(JSON.stringify(res, null, 4))
  })
})

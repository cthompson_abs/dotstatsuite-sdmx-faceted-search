module.exports = ({ url, agencyId, version }) => ({
  data: {
    dataflows: [
      {
        id: 'DF1',
        version,
        agencyID: agencyId,
      },
      {
        id: 'DF2',
        version,
        agencyID: agencyId,
        isExternalReference: true,
        links: [
          {
            href: `${url}/external/dataflow/${agencyId}/DF2/${version}`,
            rel: 'external',
          },
        ],
      },
      {
        id: 'DF3',
        version: '1.0',
        agencyID: agencyId,
      },
      {
        id: 'DF4',
        version: '1.0',
        agencyID: agencyId,
      },
    ],
  },
});

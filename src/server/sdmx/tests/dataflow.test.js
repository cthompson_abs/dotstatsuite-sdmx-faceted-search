import Parser from '../dataflow'
import emptyConstraint from '../../tests/mocks/sdmx/empty_constraint'
import AIR_EMISSIONS_DF from '../../tests/mocks/sdmx/dataflow_AIR_EMISSIONS_DF'

const mockedDate = new Date(2017, 11, 10)
global.Date = jest.fn(() => mockedDate)
global.Date.now = global.Date

const defaultParserOptions = { datasourceId: 'TEST', dimensionValuesLimit: 1000 }

describe('SDMX | dataflow', () => {
  const ref = { agencyID: 'OECD.CFE', id: 'DF_TOURISM_ENT_EMP', version: '1.0' }
  it('should make dataflows (abs)', () => {
    const parser = Parser(require('../../tests/mocks/sdmx/dataflow')(ref), defaultParserOptions)
    const dataflow = parser.transpile(ref)
    expect(dataflow.fields.dataflowId).toEqual('DF_TOURISM_ENT_EMP')
    expect(dataflow.fields.agencyId).toEqual('OECD.CFE')
    expect(dataflow.fields.datasourceId).toEqual('TEST')
    expect(dataflow.dimensions.length).toEqual(5)
    expect(dataflow.categories.length).toEqual(1)
    expect(dataflow.fields.name.en).toEqual('Enterprises and employment in tourism')
    expect(dataflow.fields.name.fr).toEqual('Enterprises and employment in tourism (fr)')
    expect(dataflow.fields.description.en).toEqual('Description of Enterprises and employment in tourism')
    expect(dataflow.fields.description.fr).toEqual('Description of Enterprises and employment in tourism (fr)')
    expect(dataflow.dimensions[0].name.en).toEqual('Reference area')
    expect(dataflow.dimensions[0].name.fr).toEqual('Reference area (fr)')
    // expect(dataflow.dimensions[0].children[0].name.en).toEqual('Argentina');
    // expect(dataflow.dimensions[0].children[0].name.fr).toEqual('Total (fr)');
    expect(dataflow.hasNoContraints).toBeFalsy()
    expect(JSON.stringify(dataflow)).toMatchSnapshot()
  })

  it('should make dataflows (without content constraints)', () => {
    const parser = Parser(require('../../tests/mocks/sdmx/dataflow_without_cc')(ref), defaultParserOptions)
    const dataflow = parser.transpile(ref)
    expect(JSON.stringify(dataflow)).toMatchSnapshot()
    expect(dataflow.hasNoContraints).toBeTruthy()
  })

  it('should make dataflows with data availability', () => {
    const parser = Parser(AIR_EMISSIONS_DF, defaultParserOptions)
    const dataflow = parser.transpile({ agencyID: 'OECD', id: 'AIR_EMISSIONS_DF', version: '1.0' })
    expect(JSON.stringify(dataflow)).toMatchSnapshot()
    expect(dataflow.fields.id).toEqual('TEST:AIR_EMISSIONS_DF')
    expect(dataflow.fields.agencyId).toEqual('OECD')
    expect(dataflow.fields.datasourceId).toEqual('TEST')
    expect(dataflow.dimensions.length).toEqual(2) // 2 of 3
    expect(dataflow.dimensions[0].children.length).toEqual(39) // nodes without data are kept for paths
    expect(dataflow.hasNoContraints).toBeFalsy()
  })

  it('should limit facets with dimensionValuesLimit', () => {
    const parser = Parser(AIR_EMISSIONS_DF, { datasourceId: 'TEST', dimensionValuesLimit: 5 })
    const dataflow = parser.transpile({ agencyID: 'OECD', id: 'AIR_EMISSIONS_DF', version: '1.0' })
    expect(JSON.stringify(dataflow)).toMatchSnapshot()
    expect(dataflow.fields.id).toEqual('TEST:AIR_EMISSIONS_DF')
    expect(dataflow.fields.agencyId).toEqual('OECD')
    expect(dataflow.fields.datasourceId).toEqual('TEST')
    expect(dataflow.dimensions.length).toEqual(1) // -1 CC, -1 limit, 1 of 3
  })

  it('should have empty constraints', () => {
    const parser = Parser(emptyConstraint, defaultParserOptions)
    const dataflow = parser.transpile({ agencyID: 'OECD.SDD', id: 'NAMAIN_T0111_A', version: '3.0' })
    expect(JSON.stringify(dataflow)).toMatchSnapshot()
    expect(dataflow.hasEmptyConstraints).toBeTruthy()
    expect(dataflow.hasNoContraints).toBeFalsy()
  })
})

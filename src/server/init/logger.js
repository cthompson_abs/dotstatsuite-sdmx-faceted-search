import debug from '../debug'
import queue from 'queue'
import { ERROR, WARNING } from '../reporter/errors'

const log = ({ type, loading: { loadingId, message }}) => {
  const types = {ERROR: 'error', WARNING: 'warn'}
  const fn = debug[types[type] ?? 'info'].bind(debug)
  if(message){
    if(!type || [ERROR, WARNING].includes(type)) fn({ loadingId }, message)
    else fn({ loadingId }, `${type.toLowerCase().replaceAll('_', ' ')}: ${message}`)
  }
}

const init = async ctx => {
  const { models, report } = ctx()
  var q = queue({ concurrency: 1, autostart: true })
  report.listen((state, action) => q.push(() => models.jobs.manageEvent(action, state)))
  report.listen((state, action) => log(action))
  debug.info('logger is ready')
  return ctx
}

export default init

import R from 'ramda';

const getVerb = req => req.method.toLowerCase();
const getInput = req => R.mergeAll([req.query, req.body]);
const getFullMethod = verb => verb;

export const getMessage = (req, service) => {
  const input = getInput(req);
  const verb = getVerb(req);
  return { service, method: getFullMethod(verb), input };
};

const server = (service, evtx) => (req, res, next) => {
  if (!evtx) return next(new Error('EvtX is not configured!'));
  evtx
    .run(getMessage(req, service), { req })
    .then(data => res.json(data))
    .catch(next);
};

export default server;

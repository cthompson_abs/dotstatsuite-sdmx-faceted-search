import R from 'ramda';
import { UnknownMethodError, UnknownServiceError } from 'evtx';
import { HTTPError } from '../../../utils/errors';

const getVerb = req => req.method.toLowerCase();
const getInput = req => R.mergeAll([req.query, req.body]);
const getFullMethod = (verb, method) => (method ? `${verb}$${method}` : verb);
const parseUrl = url => {
  const re = new RegExp(/^\/+(\w+)\/*(\w*)/);
  const [_, service, method] = re.exec(url); // eslint-disable-line no-unused-vars
  return R.map(x => x || undefined, [service, method]);
};

export const getMessage = req => {
  const [service, method] = parseUrl(req.path);
  const input = getInput(req);
  const verb = getVerb(req);
  return { service, method: getFullMethod(verb, method), input };
};

const server = evtx => (req, res, next) => {
  if (!evtx) return next(new Error('EvtX is not configured!'));
  evtx
    .run(getMessage(req), { req, res, tenant: req.tenant })
    .then(data => {
      if (data) res.json(data);
    })
    .catch(err => {
      if (err instanceof UnknownMethodError || err instanceof UnknownServiceError)
        return next(new HTTPError(404, 'service not found'));
      return next(err);
    });
};

export default server;

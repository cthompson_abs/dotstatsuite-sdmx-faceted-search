import util from 'util';
import initSolr from '../dist/server/solr';
import initConfig from '../dist/server/config';
import data from '../data/data.json';

const p = data => console.log(util.inspect(data, false, null));
initConfig()
  .then(initSolr)
  .then(({ solr }) =>
    solr
      .deleteAll()
      .then(() => solr.add(data))
      .then(() => solr.select({ query: '*:*' }))
      .then(p),
  )
  .catch(console.log);

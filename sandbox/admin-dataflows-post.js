import axios from 'axios';

const url = 'http://localhost:5434';
const key = 'secret';

const run = () =>
  axios
    .post(
      `${url}/admin/dataflows?mode=sync`,
      {},
      { headers: { 'x-api-key': key, 'x-tenant': 'oecd-pp' } },
    )
    .then(res => [res.status, res.data])
    .catch(res => [res.response.status, res.response.data])

run().then(x => console.log(JSON.stringify(x, null, 4))).then(() => process.exit());

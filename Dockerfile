FROM node:18-alpine

ARG GIT_HASH

RUN mkdir -p /sfs
WORKDIR /sfs

RUN echo $GIT_HASH

COPY dist /sfs/dist
COPY scripts /sfs/scripts
COPY data /sfs/data
COPY package.json yarn.lock /sfs/

RUN yarn install --production && yarn cache clean

ENV GIT_HASH=$GIT_HASH

EXPOSE 80
CMD yarn dist:run
